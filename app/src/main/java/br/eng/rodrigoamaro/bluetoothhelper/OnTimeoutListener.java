package br.eng.rodrigoamaro.bluetoothhelper;

public interface OnTimeoutListener {

    void onTimeout();

}
