package br.eng.rodrigoamaro.bluetoothhelper;


import android.content.Context;

public interface ContextProvider {
    Context getContext();
}
